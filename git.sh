#!/bin/bash

source $(dirname $0)/common.sh

function git_branch_to_ref() {
    local branch=$1
    echo refs/remotes/$REMOTE/$branch
}

function git_create_branch() {
    local from_branch=$1
    local to_branch=$2

    local from_ref=$(git_branch_to_ref $from_branch)
    local to_ref=$(git_branch_to_ref $to_branch)

    $DRY_RUN git update-ref $to_ref $from_ref
    $DRY_RUN git push --quiet $REMOTE $to_ref:refs/heads/$to_branch
}

function git_branch_hash() {
    local branch=$1

    git show-ref --hash $(git_branch_to_ref $branch)
}

function git_fetch_branches() {
    git fetch --prune $REMOTE
}

function git_fetch_soft_fork() {
    git fetch --prune $REMOTE +refs/soft-fork/*:refs/soft-fork/*
}

function git_fetch() {
    git_fetch_branches
    git_fetch_soft_fork
}

function git_visible_soft_fork() {
    local date=${1:-'[0-9-]+'}
    git show-ref | sed -n -Ee "s|.* refs/remotes/$REMOTE/(soft-fork/$date/.*)|\1|p" | sort
}

function git_branch_exists() {
    local branch=$1

    git show-ref --quiet $(git_branch_to_ref $branch)
}

function git_hidden_soft_fork() {
    local date=${1:-'[0-9-]+'}
    git show-ref | sed -n -Ee "s|.* refs/(soft-fork/$date/.*)|\1|p" | sort
}

function git_get_dates() {
     sed -n -Ee "s|.*soft-fork/([0-9-]+).*|\1|p" | sort -ru
}

function git_get_commits() {
    local base_ref=$1
    local head_ref=$2

    git log --no-merges --reverse --format=%H $base_ref..$head_ref
}

function git_patch_id() {
    local ref="$1"

    git show "$ref" | git patch-id | cut -f1 -d' '
}
