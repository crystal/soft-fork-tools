#!/bin/bash

set -ex

source $(dirname $0)/archive-branches-test.sh
source $(dirname $0)/api-test.sh
source $(dirname $0)/rebase-test.sh

trap "teardown" EXIT

teardown
install_packages
set_trace

function tests() {
    archive_branches_tests
}
    
${@:-tests}
