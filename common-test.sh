#!/bin/bash

source $(dirname $0)/common.sh

function test_update_readme() {
    local content="$1"

    echo "$content" > README.txt
    git add README.txt
    git commit -m "README $content"
}

function test_repositories_mock() {
    (
	cd $TMP_DIR
	mkdir origin
	(
	    cd origin
	    git init -b main .
	    git config user.email someone@example.com
	    git config user.name 'Some One'
	    test_update_readme main

	    git checkout -b release/$STABLE main
	    test_update_readme release/$STABLE

	    for b in $SOFTWARE $FEATURES ; do
		git checkout -b $b main
		test_update_readme $b
	    done
	    
	    for b in $STABLE_FEATURES ; do
		git checkout -b $STABLE/$b release/$STABLE
		test_update_readme $STABLE/$b
	    done
	)
        git clone origin work
	(
	    :
	)
    )
}

function test_forgejo_setup() {
    test_repositories_mock
    (
	cd $TMP_DIR/origin
	git checkout main
	git remote add $REMOTE $FORGEJO_SCHEME://$CI_REPO_OWNER:$FORGEJO_TOKEN@$FORGEJO_INSTANCE/$CI_REPO_OWNER/$CI_REPO_NAME
	git push -u  $REMOTE main
	git rev-parse HEAD > $TMP_DIR/commit
	git push --all 
    )
}
