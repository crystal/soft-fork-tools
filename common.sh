#!/bin/bash

: ${SOFTWARE:=forgejo}
: ${FEATURES:=forgejo-i18n forgejo-branding forgejo-privacy forgejo-a11y forgejo-development forgejo-ci}
: ${STABLE:=v1.18}
: ${STABLE_FEATURES:=forgejo-i18n forgejo-branding forgejo-privacy forgejo-a11y forgejo-development forgejo-ci}
: ${UPSTREAM_BRANCHES:=main release/v1.18}
: ${REMOTE:=origin}

declare -A BASE_BRANCH=(
    forgejo-i18n                forgejo-development
    forgejo-branding		forgejo-development
    forgejo-privacy		forgejo-development
    forgejo-a11y		forgejo-development
    forgejo-development		forgejo-ci
    forgejo-ci			main

    v1.18/forgejo-i18n          v1.18/forgejo-development
    v1.18/forgejo-branding      v1.18/forgejo-development
    v1.18/forgejo-privacy       v1.18/forgejo-development
    v1.18/forgejo-a11y          v1.18/forgejo-development
    v1.18/forgejo-development   v1.18/forgejo-ci
    v1.18/forgejo-ci            release/v1.18
)

function get_gitea_branch() {
    local branch=$1
    local base_branch="${BASE_BRANCH[$branch]}"
    if test "$base_branch"; then
	get_gitea_branch $base_branch
    else
	echo $branch
    fi
}

if test $(id -u) != 0 ; then
    SUDO=sudo
fi

TMP_DIR=$(mktemp -d)
TODAY=$(date +%F)

function cleanup_tmp() {
    rm -fr ${TMP_DIR}
    TMP_DIR=$(mktemp -d)
}

function teardown() {
    for f in $(set | sed -n -e 's/^\([0-9a-z_]*_teardown\) .*/\1/p'); do
	$f || true
    done
    cleanup_tmp
}

function comment_file() {
    cat "$1" | sed -e 's/^/# /'
}

function comment_header() {
    echo "#"
    comment_line "$@"
    echo "#"
}

function comment_line() {
    echo "# $@"
}

function dry_run() {
    echo "$@"
}

function install_packages() {
    local packages="jq git"
    if ! which $packages >/dev/null ; then
	$SUDO apt-get update --quiet
	$SUDO apt-get install -qq -y $packages
    fi
}

function set_trace() {
#    shopt -s -o xtrace
    PS4='${BASH_SOURCE[0]}:$LINENO: ${FUNCNAME[0]}:  '
}

