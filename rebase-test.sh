#!/bin/bash

source $(dirname $0)/common-test.sh
source $(dirname $0)/rebase.sh

function rebase_create_branch_test() {
    test_forgejo_setup
    TODAY=2022-01-20 archive_branches $TMP_DIR/origin

    (
	cd $TMP_DIR/origin
	local branch=forgejo-ci

	rebase_base_branch_sanity_check $branch
	git checkout main
	test_update_readme "development happened upstream"
	git push --all
	! rebase_base_branch_sanity_check $branch

	local head_branch=$(rebase_head_branch $branch)
	! git_branch_exists $head_branch
	rebase_create_branch $branch
	git_branch_exists $head_branch
	rebase_create_branch $branch
    )
}

function rebase_create_pr_test() {
    test_forgejo_setup
    TODAY=2022-01-20 archive_branches $TMP_DIR/origin

    (
	cd $TMP_DIR/origin
	local branch=forgejo-ci
	git checkout main
	test_update_readme "development happened upstream"
	git push --all
	rebase_create_branch $branch

	local base_branch=$(rebase_base_branch $branch)
	local head_branch=$(rebase_head_branch $branch)

	! api_pr_exists $base_branch $head_branch
	rebase_create_pr $branch
	api_pr_exists $base_branch $head_branch
    )
}

function rebase_switch_test() {
    test_forgejo_setup

    (
	cd $TMP_DIR/origin
	local branch=forgejo-ci
	local base_branch=$(rebase_base_branch $branch)
	local head_branch=$(rebase_head_branch $branch)

	! git show-ref --quiet refs/heads/$head_branch
	! test $(git branch --show-current) = $head_branch
	rebase_switch $branch
	git show-ref --quiet refs/heads/$head_branch
	test $(git branch --show-current) = $head_branch

	git switch main
	
	! test $(git branch --show-current) = $head_branch
	rebase_switch $branch
	test $(git branch --show-current) = $head_branch
	rebase_switch $branch
	test $(git branch --show-current) = $head_branch
    )
}

function rebase_cherry_pick_test() {
    test_forgejo_setup
    TODAY=2022-01-20 archive_branches $TMP_DIR/origin

    (
	cd $TMP_DIR/origin
	local branch=forgejo-ci
	rebase_create_branch $branch
	rebase_switch $branch

	test "$(rebase_get_commits_to_cherry_pick $branch | wc -l)" = 1
	test "$(rebase_get_commits_in_the_local_branch $branch | wc -l)" = 0
	rebase_cherry_pick $branch > $TMP_DIR/cherry-pick
	grep --quiet "local branch is empty" $TMP_DIR/cherry-pick
	test "$(rebase_get_commits_in_the_local_branch $branch | wc -l)" = 1

	rebase_cherry_pick $branch > $TMP_DIR/cherry-pick
	grep --quiet "already cherry-picked" $TMP_DIR/cherry-pick

	test_update_readme "added one more commit"
	rebase_cherry_pick $branch > $TMP_DIR/cherry-pick
	grep --quiet "commits are different" $TMP_DIR/cherry-pick
    )
}

function rebase_check_status_test() {
    test_forgejo_setup
    TODAY=2022-01-20 archive_branches $TMP_DIR/origin

    (
	cd $TMP_DIR/origin
	local branch=forgejo-ci
	rebase_create_branch $branch

	local head_hash=$(git_branch_hash $(rebase_head_branch $branch))
	api_set_status $head_hash success
	rebase_check_status $branch

	api_set_status $head_hash failure
	! rebase_check_status $branch
   )
}

function rebase_feature_branch_test() {
    test_forgejo_setup
    TODAY=2022-01-20 archive_branches $TMP_DIR/origin

    (
	cd $TMP_DIR/origin
	local branch=forgejo-ci
	local branch_hash=$(git_branch_hash $branch)

	rebase_create_branch $branch
	rebase_switch $branch
	rebase_cherry_pick $branch
	rebase_create_pr $branch

	local head_branch=$(rebase_head_branch $branch)
	local head_hash=$(git_branch_hash $head_branch)
	api_set_status $head_hash success

	rebase_check_status $branch
	rebase_force_push $branch
	rebase_close_pr $branch

	test $branch_hash != "$(git_branch_hash $branch)"
	git branch -v | cat
	git rev-parse HEAD
	test "$(git show-ref --hash refs/heads/$head_branch)" = "$(git_branch_hash $branch)"

	rebase_delete_branch $branch

	! git_branch_exists $head_branch
    )
}
