#!/bin/bash

source $(dirname $0)/common.sh
source $(dirname $0)/git.sh

ARCHIVE_KEEP=1d

function archive_branch() {
    local archive=$1
    local branch=$2
    local archived_branch=$archive/$branch

    if ! git_branch_exists $archived_branch ; then
	comment_line "Archive $branch as $archived_branch"
	git_create_branch $branch $archived_branch
    fi
}

function archive_hide() {
    local date=$1

    comment_header "Convert branches soft-fork/$date/* into references invisible to the user"
    git_visible_soft_fork $date | while read branch ; do
	local hidden=refs/$branch
	local visible=refs/remotes/$REMOTE/$branch
	comment_line "Convert branch $visible into reference $hidden"
	if ! git show-ref --quiet $hidden ; then
	    $DRY_RUN git update-ref $hidden $visible
	    $DRY_RUN git push --quiet $REMOTE +$hidden:$hidden
	fi
	$DRY_RUN git push --delete $REMOTE refs/heads/$branch
    done
}

function archive_current_branches() {
    comment_header "Archive all branches as soft-fork/$TODAY/*"
    archive_action_on_branches archive_branch
}

function archive_action_on_branches() {
    local action=$1

    local archive=soft-fork/$TODAY
    for branch in $SOFTWARE $FEATURES $UPSTREAM_BRANCHES ; do
	$action $archive $branch
    done
    for branch in $STABLE_FEATURES ; do
	$action $archive $STABLE/$branch
    done
}

function archive_hide_branches() {
    git_visible_soft_fork | git_get_dates | sed -e "$ARCHIVE_KEEP" | while read old ; do
	archive_hide $old
    done
}

function archive_branches() {
    local work=${1:-.}

    install_packages

    (
	cd $work
	git_fetch
	archive_current_branches
	archive_hide_branches
    )
}
