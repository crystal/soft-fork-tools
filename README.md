In support of [the documented Forgejo workflow](https://codeberg.org/forgejo/forgejo/src/branch/forgejo/CONTRIBUTING/WORKFLOW.md).

# Archive branches

`REMOTE=forgejo DRY_RUN=dry_run bash ~/software/forgejo/soft-fork-tools/main.sh archive_branches` and copy paste the output in a shell

# WIP: Rebase branches

CI_REPO_OWNER=root CI_REPO_NAME=other FORGEJO_INSTANCE=0.0.0.0:8785 FORGEJO_SCHEME=http FORGEJO_TOKEN=xyz REMOTE=origin bash ~/software/forgejo/soft-fork-tools/main.sh rebase_feature_branch v1.18/forgejo-ci

